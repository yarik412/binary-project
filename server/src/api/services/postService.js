import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const updatePost = (id,body) =>{
  console.log("res")
  const data = postRepository.updateById(id,{body});
  return data;
};

export const deletePost = (id) =>{
  console.log("delete",id);
  const data = postRepository.updateById(id,{isDelete:true});
  return data;
};

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => {
    if(isLike){
      if(react.isLike === true){
        return postReactionRepository.deleteById(react.id);
      }else{
        return postReactionRepository.updateById(react.id, { isLike, prevReact:true });
      }
    }else{
      if(react.isLike === false){
        return postReactionRepository.deleteById(react.id);
      }else{
        return postReactionRepository.updateById(react.id, { isLike, prevReact:false });
      }
    }
  };

  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  
  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
