import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Form, Header, Button } from 'semantic-ui-react';
import { postType } from 'src/common/propTypes';
import { editPost } from 'src/containers/Thread/actions';

function EditPost({ showEditPost, editPost: edit, post }) {
  const [postBody, setPostBody] = useState('');
  const { id } = post;

  const handleEditPost = async () => {
    if (!postBody) {
      return;
    }
    await edit({ postId: id, body: postBody });
    setPostBody('');
  };

  return (
    <Modal as={Form} onSubmit={handleEditPost} open size="tiny" onClose={() => showEditPost()}>
      <Header icon="pencil" content="Change your post" as="h2" />
      <Modal.Content>
        <Form.TextArea
          style={{ width: '100%' }}
          value={postBody}
          focus
          placeholder="Set new text for post"
          required
          onChange={ev => setPostBody(ev.target.value)}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button type="button" color="red" content="Close" onClick={() => showEditPost()} />
        <Button type="submit" color="green" content="Save" primary />
      </Modal.Actions>
    </Modal>
  );
}

EditPost.propTypes = {
  post: postType.isRequired,
  editPost: PropTypes.func.isRequired,
  showEditPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editPostInform
});

const actions = { editPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPost);
